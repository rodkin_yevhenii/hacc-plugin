<?php

use Hacc\Inc\Models\Member;
use Hacc\Inc\Models\Family;

$member  = Member::getInstance(get_current_user_id());
$family  = $member->getFamily();

if (! empty($family)) {
    if (! empty($_POST['action'])) {
        switch ($_POST['action']) {
            case 'add_member':
                $family->addMember($_POST['family']['member_id']);
                break;
            case 'remove_member':
                $family->removeMember($_POST['family']['member_id']);
                break;
            case 'edit_family':
                $family->setTitle($_POST['family']['title']);
                break;
        }
        $family->save();
    }
} elseif (! empty($_POST['action']) && 'create_family' === $_POST['action']) {
    Family::createFamily(sanitize_text_field($_POST['family']['title']), (int) $_POST['family']['member_id']);
    $family = $member->getFamily();
}

?>

<?php if (! $family) : ?>
    <h1><?= __('Create Family', 'hacc'); ?></h1>
    <form method="post">
        <input type="text" placeholder="<?= __('Title', 'hacc'); ?>" name="family[title]">
        <input type="text" placeholder="<?= __('Member ID', 'hacc'); ?>" name="family[member_id]">
        <input type="hidden" name="action" value="create_family">
        <input type="submit" value="<?= __('Create Family', 'hacc'); ?>">
    </form>
<?php else : ?>
    <h1><?= $family->getTitle(); ?></h1>
    <div>
        <?php
        $members = $family->getMembers();
        ?>
        <h3><?= __('Members list', 'hacc'); ?>:</h3>
        <ul>
            <?php foreach ($members as $member) : ?>
                <?php /** @var $member Member */ ?>
                <li><?= $member->getName(); ?>: <?= $member->getID(); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div>
        <h3><?= __('Add new member to your family', 'hacc'); ?>:</h3>
        <form method="post">
            <input type="text" placeholder="<?= __('Member ID', 'hacc'); ?>" name="family[member_id]">
            <input type="hidden" name="action" value="add_member">
            <input type="submit" value="<?= __('Add member', 'hacc'); ?>">
        </form>
    </div>
    <div>
        <h3><?= __('Remove member from your family', 'hacc'); ?>:</h3>
        <form method="post">
            <input type="text" placeholder="<?= __('Member ID', 'hacc'); ?>" name="family[member_id]">
            <input type="hidden" name="action" value="remove_member">
            <input type="submit" value="<?= __('Remove member', 'hacc'); ?>">
        </form>
    </div>
    <div>
        <h3><?= __('Edit family title', 'hacc'); ?>:</h3>
        <form method="post">
            <input type="text" placeholder="<?= __('Title', 'hacc'); ?>" name="family[title]">
            <input type="hidden" name="action" value="edit_family">
            <input type="submit" value="<?= __('Update family', 'hacc'); ?>">
        </form>
    </div>
<?php endif; ?>
