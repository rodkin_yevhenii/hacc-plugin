<div id="create-wallet-success" class="alert alert-success alert-dismissible" role="alert" style="display: none">
    <strong><?= __('Success', 'hacc') ?>!</strong> <?= __('New wallet was created', 'hacc') ?>.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div id="create-wallet-error" class="alert alert-danger alert-dismissible" role="alert" style="display: none">
    <strong><?= __('Failed', 'hacc') ?>...</strong> <?= __('New wallet wasn\'t created', 'hacc') ?>.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form method="post" id="create-wallet-form">
    <div class="form-group">
        <label for="newWalletTitle"><?= __('Title', 'hacc') ?></label>
        <input type="text" name="title" class="form-control" id="newWalletTitle"
           aria-describedby="newWalletTitleHelp">
    </div>

    <div class="form-group">
        <label for="newWalletBalance"><?= __('Balance', 'hacc') ?></label>
        <input type="text" name="balance" class="form-control" id="newWalletBalance"
               aria-describedby="newWalletBalanceHelp">
    </div>

    <?php if (current_user_can('manage_options')) : ?>
        <div class="form-group">
            <label for="newWalletOwner"><?= __('Owner ID', 'hacc') ?></label>
            <input type="text" name="owner" class="form-control" id="newWalletOwner"
                   aria-describedby="newWalletOwnerHelp">
        </div>
    <?php endif; ?>

    <fieldset class="form-group js_new-wallet-type">
        <div class="row">
            <legend class="col-form-label col-sm-5 col-lg-4 pt-0"><?= __('Account type', 'hacc'); ?></legend>
            <div class="col-sm-7 col-lg-8">
                <div class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input" type="radio" name="type" id="newRegularWallet"
                           value="regular" checked>
                    <label class="custom-control-label" for="newRegularWallet">
                        <?= __('Regular', 'hacc'); ?>
                    </label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input" type="radio" name="type" id="newCreditWallet"
                           value="credit">
                    <label class="custom-control-label" for="newCreditWallet">
                        <?= __('Credit', 'hacc'); ?>
                    </label>
                </div>
            </div>
        </div>
    </fieldset>

    <div class="form-group js_credit_limit" style="display: none">
        <label for="newWalletLimit"><?= __('Credit limit.', 'hacc'); ?></label>
        <input type="text" name="limit" class="form-control" id="newWalletLimit">
    </div>

    <fieldset class="form-group">
        <div class="row">
            <legend class="col-form-label col-sm-5 col-lg-4 pt-0">
                <?= __('Other family members can show the wallet?', 'hacc'); ?>
            </legend>
            <div class="col-sm-7 col-lg-8">
                <div class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input" type="radio" name="visibility"
                           id="newWalletViliblity1" value="true" checked>
                    <label class="custom-control-label" for="newWalletViliblity1">
                        <?= __('Yes', 'hacc'); ?>
                    </label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input" type="radio" name="visibility"
                           id="newWalletViliblity2" value="false">
                    <label class="custom-control-label" for="newWalletViliblity2">
                        <?= __('No', 'hacc'); ?>
                    </label>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset class="form-group">
        <div class="row">
            <legend class="col-form-label col-sm-5 col-lg-4 pt-0">
                <?= __('Other family members use the wallet?', 'hacc'); ?>
            </legend>
            <div class="col-sm-7 col-lg-8">
                <div class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input" type="radio" name="usage"
                           id="newWalletUsage1" value="true" checked>
                    <label class="custom-control-label" for="newWalletUsage1">
                        <?= __('Yes', 'hacc'); ?>
                    </label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input" type="radio" name="usage"
                           id="newWalletUsage2" value="false">
                    <label class="custom-control-label" for="newWalletUsage2">
                        <?= __('No', 'hacc'); ?>
                    </label>
                </div>
            </div>
        </div>
    </fieldset>

    <button type="submit" id="create_wallet" class="btn btn-primary mb-2 js_create_wallet">
        <?= __('Create new Wallet', 'hacc'); ?>
        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true" style="display: none;"></span>
    </button>
</form>
