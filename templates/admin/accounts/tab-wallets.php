<?php

use Hacc\Inc\Models\Member;
use Hacc\Inc\Models\Wallet;

$member = Member::getInstance(get_current_user_id());
$counter = 0;
$defaultAccount = false;
?>

<div class="row">
    <div class="col-4">
        <ul class="list-group list-group-flush list-group-js wallets_list js_wallets_list">
            <?php foreach ($member->getFamily()->getWallets('use') as $id => $account) : ?>
                <?php /** @var Wallet $account */ ?>

                <?php if (! $defaultAccount) : ?>
                    <?php $defaultAccount = $account ?>
                <?php endif; ?>
                <li id="<?= $id; ?>"
                    class="list-group-item list-group-item-action wallet js_wallet <?= empty($counter) ? 'active' : ''; ?>">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1"><span class="title"><?= $account->getTitle() ?></span></h5>
                        <span>
                            <span class="balance"><?= $account->getBalance() ?></span>
                            <small>UAH</small>
                        </span>
                    </div>
                    <small><?= $account->getOwner()->getName() ?></small>
                </li>
                <?php $counter ++; ?>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="col-8">
        <div class="accordion" id="accordionWallet">
            <div class="card">
                <div class="card-header" id="headingInfo">
                    <h5 class="mb-0">
                        <span class="w-100 text-center" type="button" data-toggle="collapse" data-target="#collapseInfo"
                                aria-expanded="true" aria-controls="collapseInfo">
                            <?= __('Information', 'hacc') ?>
                        </span>
                    </h5>
                </div>

                <div id="collapseInfo" class="collapse show" aria-labelledby="headingInfo"
                     data-parent="#accordionWallet">
                    <div class="card-body">
                        <ul id="wallet-info">
                            <li>
                                <span class="title"><?= __('Balance', 'hacc') ?>:</span>
                                <span class="amount">
                                    <?= $defaultAccount->getBalance(); ?>
                                    <small><?= __('UAH', 'hacc') ?></small>
                                </span>
                            </li>

                            <?php if ('credit' === $defaultAccount->getType()) : ?>
                                <li>
                                    <span class="title"><?= __('Credit limit', 'hacc') ?>:</span>
                                    <span class="amount">
                                        <?= $defaultAccount->getCreditLimit(); ?>
                                        <small><?= __('UAH', 'hacc') ?></small>
                                    </span>
                                </li>
                                <li>
                                    <span class="title"><?= __('Own money', 'hacc') ?>:</span>
                                    <span class="amount">
                                        <?= $defaultAccount->getOwnMoney(); ?>
                                        <small><?= __('UAH', 'hacc') ?></small>
                                    </span>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                        <span class="w-100 text-center collapsed" type="button" data-toggle="collapse"
                              data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <?= __('Transfer money to another wallet', 'hacc') ?>
                        </span>
                    </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionWallet">
                    <div class="card-body">

                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                        <span class="w-100 text-center collapsed" type="button" data-toggle="collapse"
                              data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <?= __('Recharge balance', 'hacc') ?>
                        </span>
                    </h5>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionWallet">
                    <div class="card-body">

                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingFour">
                    <h5 class="mb-0">
                        <span class="w-100 text-center collapsed" type="button" data-toggle="collapse"
                              data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            <?= __('Recharge balance', 'hacc') ?>
                        </span>
                    </h5>
                </div>
                <div id="collapseFour" class="collapse" aria-labelledby="headingThree" data-parent="#accordionWallet">
                    <div class="card-body">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

