<?php

use Hacc\Inc\Base\BaseController;

?>
<div class="container wallets">
    <div class="row">
        <div class="col text-center">
            <h1>Wallets</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <ul class="list-group list-group-horizontal list-group-js wallets-tabs" id="wallets-list-tab" role="tablist">
                <li class="list-group-item list-group-item-action text-center active" data-toggle="list" href="#wallets" role="tab">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1 mx-auto">
                            <span class="title"><?= __('Wallets', 'hacc') ?></span>
                        </h5>
                    </div>
                </li>
                <li class="list-group-item list-group-item-action text-center" data-toggle="list" href="#create-wallet" role="tab">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1 mx-auto">
                            <span class="title"><?= __('Create wallet', 'hacc') ?></span>
                        </h5>
                    </div>
                </li>
                <li class="list-group-item list-group-item-action text-center" data-toggle="list" href="#history" role="tab">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1 mx-auto">
                            <span class="title"><?= __('History', 'hacc') ?></span>
                        </h5>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-12">
            <div class="tab-content" id="wallets-nav-tabContent">
                <div class="tab-pane fade show active" id="wallets" role="tabpanel">
                    <?php require_once BaseController::getPluginPath() . '/templates/admin/accounts/tab-wallets.php'; ?>
                </div>
                <div class="tab-pane fade" id="create-wallet" role="tabpanel">
                    <?php require_once BaseController::getPluginPath() . '/templates/admin/accounts/tab-create-account.php'; ?>
                </div>
                <div class="tab-pane fade" id="history" role="tabpanel">

                </div>
            </div>
        </div>


    </div>
</div>

<div style="display: none">
    <div class="col-auto tabs">
        <a href="#wallets" class="tab active">Wallets</a>
        <a href="#remittance" class="tab">Remittance</a>
        <a href="#create" class="tab">Create wallet</a>
        <a href="#settings" class="tab">Settings</a>
    </div>
    <div class="col content">
        <div id="wallets">
            <?php
            require_once BaseController::getPluginPath() . '/templates/admin/accounts/tab-wallets.php';
            ?>
        </div>
        <div id="remittance" style="display: none"><h2>Remittance</h2></div>
        <div id="create" style="display: none">
            <h2>Create wallet</h2>
            <?php
            require_once BaseController::getPluginPath() . '/templates/admin/accounts/tab-create-account.php';
            ?>
        </div>
        <div id="settings" style="display: none">
            <h2>Settings</h2>
        </div>
    </div>
</div>
