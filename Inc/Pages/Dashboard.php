<?php

namespace Hacc\Inc\Pages;

use Hacc\Inc\Api\Callbacks\AdminCallbacks;

/**
 * Class Dashboard
 *
 * @author Rodkin Yevhenii
 * @package Hacc\Inc\Pages
 */
class Dashboard
{
    /**
     * Main page data.
     *
     * @var array
     */
    protected static $page;

    /**
     * Subpages data.
     *
     * @var array
     */
    protected static $subPages;

    /**
     * Dashboard constructor.
     */
    public function __construct()
    {
        self::setPage();
        self::setSubPages();

        add_action('admin_menu', [__CLASS__, 'registerPages']);
    }

    /**
     * Set main dashboard page data.
     */
    protected static function setPage()
    {
        self::$page = [
            'page_title' => 'Home Accounting',
            'menu_title' => 'Accounting',
            'capability' => 'manage_options',
            'menu_slug'  => 'hacc',
            'callback'   => [AdminCallbacks::class, 'adminDashboard'],
            'icon_url'   => 'dashicons-admin-home',
            'position'   => 110
        ];
    }

    protected static function setSubPages()
    {
        self::$subPages = [
            [
                'parent_slug' => self::$page['menu_slug'],
                'page_title'  => self::$page['page_title'],
                'menu_title'  => 'Dashboard',
                'capability'  => self::$page['capability'],
                'menu_slug'   => self::$page['menu_slug'],
                'callback'    => self::$page['callback']
            ],
            [
                'parent_slug' => self::$page['menu_slug'],
                'page_title'  => __('My Family', 'hacc'),
                'menu_title'  => __('Family', 'hacc'),
                'capability'  => 'manage_options',
                'menu_slug'   => 'hacc-family',
                'callback'    => [AdminCallbacks::class, 'adminFamily']
            ],
            [
                'parent_slug' => self::$page['menu_slug'],
                'page_title'  => __('My Accounts', 'hacc'),
                'menu_title'  => __('Wallets', 'hacc'),
                'capability'  => 'manage_options',
                'menu_slug'   => 'hacc-accounts',
                'callback'    => [AdminCallbacks::class, 'adminAccounts']
            ],
        ];
    }

    public static function registerPages()
    {
        /**
         * Register main page.
         */
        add_menu_page(
            self::$page['page_title'],
            self::$page['menu_title'],
            self::$page['capability'],
            self::$page['menu_slug'],
            self::$page['callback'],
            self::$page['icon_url'],
            self::$page['position']
        );

        foreach (self::$subPages as $subPage) {
            add_submenu_page(
                $subPage['parent_slug'],
                $subPage['page_title'],
                $subPage['menu_title'],
                $subPage['capability'],
                $subPage['menu_slug'],
                $subPage['callback']
            );
        }
    }
}
