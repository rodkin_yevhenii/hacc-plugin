<?php

namespace Hacc\Inc;

use Hacc\Inc\Base\Enqueue;
use Hacc\Inc\Base\PostTypes;
use Hacc\Inc\Pages\Dashboard;
use Hacc\Inc\Base\Ajax;

/**
 * Class Init
 *
 * @author Rodkin Yevhenii.
 * @package Hacc\Inc.
 */
class Init
{
    /**
     * Loop through the classes, initialize them.
     */
    public static function registerServices(): void
    {
        foreach (self::getServices() as $class) {
            new $class();
        }
    }

    /**
     * Store all the classes inside an array.
     *
     * @return array        Full list of classes
     */
    private static function getServices(): array
    {
        return array(
            PostTypes::class,
            Dashboard::class,
            Enqueue::class,
            Ajax::class,
        );
    }
}
