<?php

namespace Hacc\Inc\Base;

use Hacc\Inc\Api\Callbacks\AjaxCallbacks;

/**
 * Class Ajax
 *
 * @author Rodkin Yevheii <rodkin.yevhenii@gmail.com>
 * @package Hacc\Inc\Base
 */
class Ajax
{
    /**
     * Ajax constructor.
     */
    public function __construct()
    {
        $this->adminHooks();
        $this->frontendHooks();
    }

    /**
     * Register admin actions.
     */
    private function adminHooks()
    {
        add_action('wp_ajax_display_accounts_list', [AjaxCallbacks::class, 'displayAccountsList']);
        add_action('wp_ajax_display_wallet_balance_html', [AjaxCallbacks::class, 'displayWalletBalanceHtml']);
        add_action('wp_ajax_create_wallet', [AjaxCallbacks::class, 'createWallet']);
        add_action('wp_ajax_delete_wallet', [AjaxCallbacks::class, 'deleteWallet']);
        add_action('wp_ajax_transfer_money', [AjaxCallbacks::class, 'transferMoney']);
    }

    /**
     * Register frontend actions.
     */
    private function frontendHooks()
    {
        // TODOME: Use in future.
    }
}
