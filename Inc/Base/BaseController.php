<?php

namespace Hacc\Inc\Base;

/**
 * Class BaseController
 *
 * @author Rodkin Yevhenii.
 * @package Hacc\Inc\Base
 */
class BaseController
{
    /**
     * Get plugin path.
     *
     * @return string
     */
    public static function getPluginPath(): string
    {
        return plugin_dir_path(dirname(__FILE__, 2));
    }

    /**
     * Get plugin path.
     *
     * @return string
     */
    public static function getPluginUrl(): string
    {
        return plugin_dir_url(dirname(__FILE__, 2));
    }

    /**
     * Get plugin path.
     *
     * @return string
     */
    public static function getPluginName(): string
    {
        return plugin_basename(dirname(__FILE__, 3)) . '/home-accounting.php';
    }
}
