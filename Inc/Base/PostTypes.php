<?php

namespace Hacc\Inc\Base;

/**
 * Class PostTypes
 *
 * @package Hacc\Inc\Base
 */
class PostTypes
{
    /**
     * List of custom post-types.
     *
     * @var array
     */
    private static $post_types;

    /**
     * PostTypes constructor.
     */
    public function __construct()
    {
        self::addPostTypes();

        add_action('init', [ __CLASS__, 'registerPostTypes' ]);
    }

    /**
     * Add new custom post-types data.
     */
    private static function addPostTypes()
    {
        // Family post-type.
        self::$post_types['family'] = [
            'labels'    => [
                'name'          => __('Family', 'hacc'),
                'singular_name' => __('Family', 'hacc'),
                'add_new'       => __('Add new', 'hacc'),
                'add_new_item'  => __('Add new Family', 'hacc'),
                'edit_item'     => __('Edit Family', 'hacc'),
                'new_item'      => __('New Family', 'hacc'),
                'view_item'     => __('View Family', 'hacc'),
                'search_items'  => __('Search Family', 'hacc'),
                'not_found'     => __('Family not founded', 'hacc'),
                'menu_name'     => __('Families', 'hacc'),
            ],
            'menu_icon' => 'dashicons-groups',
            'supports'  => ['title'],
        ];

        // Wallet post-type.
        self::$post_types['wallet'] = [
            'labels'    => [
                'name'          => __('Wallet', 'hacc'),
                'singular_name' => __('Wallet', 'hacc'),
                'add_new'       => __('Add new', 'hacc'),
                'add_new_item'  => __('Add new Wallet', 'hacc'),
                'edit_item'     => __('Edit Wallet', 'hacc'),
                'new_item'      => __('New Wallet', 'hacc'),
                'view_item'     => __('View Wallet', 'hacc'),
                'search_items'  => __('Search Wallet', 'hacc'),
                'not_found'     => __('Wallet not founded', 'hacc'),
                'menu_name'     => __('Wallets', 'hacc'),
            ],
            'menu_icon' => 'dashicons-portfolio',
            'supports'  => ['title'],
        ];
    }

    /**
     * Register new custom post-types.
     */
    public static function registerPostTypes()
    {
        $defaultSettings = [
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => true,
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
        ];

        foreach (self::$post_types as $name => $cpt) {
            $data = array_merge($defaultSettings, $cpt);

            register_post_type($name, $data);
        }
    }
}
