<?php

namespace Hacc\Inc\Base;

/**
 * Class Enqueue
 *
 * @author Rodkin Yevhenii
 * @package Hacc\Inc\Base
 */
class Enqueue extends BaseController
{
    public function __construct()
    {
        add_action('admin_enqueue_scripts', [__CLASS__, 'enqueue']);
    }

    public static function enqueue()
    {
        // Styles.
        wp_enqueue_style(
            'bootstrap-reboot',
            self::getPluginUrl() . 'assets/css/bootstrap-reboot.css'
        );
        wp_enqueue_style(
            'bootstrap-grid',
            self::getPluginUrl() . 'assets/css/bootstrap-grid.css',
            ['bootstrap-reboot'],
            null
        );
        wp_enqueue_style(
            'bootstrap',
            self::getPluginUrl() . 'assets/css/bootstrap.css',
            ['bootstrap-reboot'],
            null
        );
        wp_enqueue_style(
            'hacc-admin-style',
            self::getPluginUrl() . 'assets/css/admin-style.css',
            ['bootstrap-reboot'],
            null
        );

        // Scripts.
        wp_enqueue_script(
            'bootstrap-bundle',
            self::getPluginUrl() . 'assets/js/bootstrap.bundle.js'
        );
        wp_enqueue_script(
            'bootstrap',
            self::getPluginUrl() . 'assets/js/bootstrap.js'
        );
        wp_enqueue_script(
            'hacc-admin-script',
            self::getPluginUrl() . 'assets/js/admin-script.js'
        );
    }
}
