<?php


namespace Hacc\Inc\Models;

use \WP_Post;

/**
 * Class Post
 *
 * @author Rodkin Yevhenii <rodkin.yevhenii@gmail.com>
 * @package Hacc\Inc\Models
 */
abstract class Post
{
    /**
     * Post ID.
     *
     * @var integer
     */
    protected $id;

    /**
     * Instance of WP_Post.
     *
     * @var WP_Post
     */
    protected $post;

    /**
     * Post title.
     *
     * @var string
     */
    protected $title;

    /**
     * Post constructor.
     *
     * @param int $id       Post ID.
     */
    public function __construct(int $id = 0)
    {
        if (! empty($id)) {
            $this->id    = $id;
            $this->post  = get_post($id);
            $this->title = $this->post->post_title;
        }
    }

    /**
     * Get post ID.
     *
     * @return int|NULL      Post ID.
     */
    public function getId() : ?int
    {
        if (empty($this->id)) {
            return null;
        }

        return $this->id;
    }

    /**
     * Get post title.
     *
     * @return string|null
     */
    public function getTitle() : ?string
    {
        if (! empty($this->title)) {
            return $this->title;
        }

        return null;
    }

    /**
     * Set title to post.
     *
     * @param string $title         Post title.
     */
    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    /**
     * Save post data to database.
     *
     * @return int|null
     */
    abstract public function save() : ?int;
}
