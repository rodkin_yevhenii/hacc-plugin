<?php

namespace Hacc\Inc\Models;

/**
 * Class Account
 *
 * @author Rodkin Yevhenii <rodkin.yevhenii@gmail.com>
 * @package Hacc\Inc\Models
 */
class Wallet extends Post
{
    /**
     * Array with instances of this.
     *
     * @var array
     */
    protected static $instances;

    /**
     * Owner ID.
     *
     * @var int
     */
    protected $ownerId;

    /**
     * Wallet balance.
     *
     * @var float
     */
    protected $balance;

    /**
     * Account type.
     *
     * Account type must has 1 of values: "regular", "credit".
     *
     * @var string;
     */
    protected $type;

    /**
     * Allow family to see the account.
     *
     * @var boolean
     */
    protected $familyVisibility;

    /**
     * Allow family to use the account.
     *
     * @var boolean
     */
    protected $familyUsage;

    /**
     * Account constructor.
     *
     * @param int    $id        Account ID.
     */
    protected function __construct(int $id)
    {
        parent::__construct($id);

        $this->balance          = (float) get_post_meta($id, 'balance', true);
        $this->familyVisibility = (bool) get_post_meta($id, 'family_visibility', true);
        $this->familyUsage      = (bool) get_post_meta($id, 'family_usage', true);
        $this->type             = get_post_meta($id, 'type', true);
        $this->ownerId          = (int) get_post_field('post_author', $id);
    }

    /**
     * Get instance of Account by ID.
     *
     * @param int $id       Wallet ID
     *
     * @return self|null
     */
    public static function getInstance(int $id): ?self
    {
        if (! empty(static::$instances[$id])) {
            return static::$instances[$id];
        }

        global $wpdb;
        $count = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT ID FROM $wpdb->posts WHERE post_type = 'wallet' AND post_status = 'publish' AND ID = '%d'",
                $id
            )
        );

        if (empty($count)) {
            return null;
        }

        static::$instances[$id] = new static($id);

        return static::$instances[$id];
    }

    /**
     * Create new empty wallet.
     *
     * @param string $title             Wallet title.
     * @param int    $owner_id          Owner ID.
     *
     * @return self
     */
    public static function createWallet(string $title, int $owner_id = 0): ?self
    {
        $post_data = [
            'post_title'  => $title,
            'post_status' => 'publish',
            'post_type'   => 'wallet',
            'post_author' => empty($owner_id) ? get_current_user_id() : $owner_id,
        ];

        $wallet_id = wp_insert_post($post_data, true);

        if (is_wp_error($wallet_id)) {
            return null;
        }

        update_post_meta($wallet_id, 'type', 'regular');

        static::$instances[$wallet_id] = new static($wallet_id);

        return static::$instances[$wallet_id];
    }

    /**
     * Delete wallet.
     *
     * @return bool
     */
    public function deleteWallet(): bool
    {
        if (get_current_user_id() !== $this->ownerId) {
            return false;
        }

        $result = wp_delete_post($this->id);

        return (bool) $result;
    }

    /**
     * Get owner.
     *
     * @return Member|null         Instance of Member or null.
     */
    public function getOwner(): ?Member
    {
        if (empty($this->id)) {
            return null;
        }

        return Member::getInstance($this->ownerId);
    }

    /**
     * Get account type.
     * The method return 1 of values: "regular", "credit".
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Get balance.
     *
     * @return float
     */
    public function getBalance(): float
    {
        return $this->balance;
    }

    /**
     * Set balance.
     *
     * @param float $balance
     */
    public function setBalance(float $balance): void
    {
        $this->balance = $balance;
    }

    /**
     * Set account visibility status.
     *
     * @param bool $status      Account visibility status for family.
     */
    public function setFamilyVisibilityStatus(bool $status): void
    {
        $this->familyVisibility = $status;
    }

    /**
     * Set account usage status.
     *
     * @param bool $status      Account visibility status for family.
     */
    public function setFamilyUsageStatus(bool $status): void
    {
        $this->familyUsage = $status;
    }

    /**
     * Recharge balance.
     *
     * @param float $amount     Amount of money.
     *
     * @return bool             Operation result.
     */
    public function rechargeBalance(float $amount): bool
    {
        if (! $this->getOwner()->getFamily()->isMember(get_current_user_id())) {
            return false;
        }

        if (get_current_user_id() !== $this->ownerId && ! $this->familyVisibility) {
            return false;
        }

        if (0 < $amount) {
            $this->balance += $amount;

            return true;
        }

        return false;
    }

    /**
     * Withdraw money from balance.
     *
     * @param float $amount     Amount of money.
     *
     * @return bool             Operation result.
     */
    public function withdrawMoney(float $amount): bool
    {
        if (! $this->getOwner()->getFamily()->isMember(get_current_user_id())) {
            return false;
        }

        if (get_current_user_id() !== $this->ownerId && ! $this->familyUsage) {
            return false;
        }

        if (0 < $amount && $this->balance > $amount) {
            $this->balance -= $amount;

            return true;
        }

        return false;
    }

    /**
     * Update post.
     *
     * @return int|null         Account ID.
     */
    public function save(): ?int
    {
        $post_data = [
            'ID'         => $this->id,
            'post_title' => $this->title,
        ];

        $post_id = wp_update_post($post_data, true);

        if (is_wp_error($post_id)) {
            return null;
        }

        update_post_meta($this->id, 'balance', $this->balance);
        update_post_meta($this->id, 'family_visibility', $this->familyVisibility);
        update_post_meta($this->id, 'family_usage', $this->familyUsage);
        update_post_meta($this->id, 'type', $this->type);

        return $post_id;
    }

    /**
     * Get visibility status for another family members.
     *
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->familyVisibility;
    }

    /**
     * Get usage status for another family members.
     *
     * @return bool
     */
    public function isUsage(): bool
    {
        return $this->familyUsage;
    }
}
