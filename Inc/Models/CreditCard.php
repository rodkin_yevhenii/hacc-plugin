<?php

namespace Hacc\Inc\Models;

/**
 * Class CreditAccount
 *
 * @author Rodkin Yevhenii <rodkin.yevhenii@gmail.com>
 * @package Hacc\Inc\Models
 */
class CreditCard extends Wallet
{
    /**
     * Default credit account balance.
     *
     * @var float
     */
    protected $creditLimit;

    /**
     * CreditAccount constructor.
     *
     * @param int $id        Account ID.
     */
    protected function __construct(int $id)
    {
        parent::__construct($id);

        $this->creditLimit = (float) get_post_meta($id, 'credit_limit', true);
    }

    /**
     * Create new empty wallet.
     *
     * @param string $title             Wallet title.
     * @param int    $owner_id          Owner ID.
     *
     * @return self
     */
    public static function createWallet(string $title, int $owner_id = 0): ?parent
    {
        $post_data = [
            'post_title'  => $title,
            'post_status' => 'publish',
            'post_type'   => 'wallet',
            'post_author' => empty($owner_id) ? get_current_user_id() : $owner_id,
        ];

        $wallet_id = wp_insert_post($post_data, true);

        if (is_wp_error($wallet_id)) {
            return null;
        }

        update_post_meta($wallet_id, 'type', 'credit');

        static::$instances[$wallet_id] = new static($wallet_id);

        return static::$instances[$wallet_id];
    }

    /**
     * Get default credit account limit.
     *
     * @return float
     */
    public function getCreditLimit(): float
    {
        return $this->creditLimit;
    }

    /**
     * Get own money.
     *
     * @return float        Own money.
     */
    public function getOwnMoney(): float
    {
        return $this->balance - $this->creditLimit;
    }

    /**
     * Set default credit account limit.
     *
     * @param float $creditLimit      Amount of money.
     *
     * @return bool                     Operation result.
     */
    public function setCreditLimit(float $creditLimit): bool
    {
        if (0 <= $creditLimit) {
            $this->creditLimit = $creditLimit;

            return true;
        }

        return false;
    }

    /**
     * Update post.
     *
     * @return int|null     Account ID.
     */
    public function save(): ?int
    {
        $post_id = parent::save();

        if (empty($post_id)) {
            return null;
        }

        update_post_meta($this->id, 'credit_limit', $this->creditLimit);

        return $post_id;
    }
}
