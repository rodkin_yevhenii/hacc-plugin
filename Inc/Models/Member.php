<?php

namespace Hacc\Inc\Models;

use \WP_Query;

/**
 * Class Member
 *
 * @author Rodkin Yevhenii <rodkin.yevhenii@gmail.com>
 * @package Hacc\Inc\Models
 */
class Member
{
    /**
     * Store of self instances.
     *
     * @var array of self.
     */
    protected static $instances;

    /**
     * Member ID.
     *
     * @var int
     */
    protected $ID;

    /**
     * Family ID.
     *
     * @var int
     */
    protected $familyId;

    /**
     * Member accounts.
     *
     * @var array of Accounts
     */
    protected $wallets;

    /**
     * Member name
     *
     * @var string
     */
    protected $name;

    /**
     * Member constructor.
     *
     * @param int $ID
     */
    protected function __construct(int $ID)
    {
        $user = get_user_by('ID', $ID);

        $this->ID       = $ID;
        $this->name     = $user->display_name;
        $this->familyId = (int) get_user_meta($ID, 'family', true);

        if (! empty($this->familyId)) {
            global $wpdb;
            $count = $wpdb->get_results(
                $wpdb->prepare(
                    "SELECT {$wpdb->usermeta}.meta_value as 'family_id' FROM $wpdb->usermeta RIGHT JOIN $wpdb->posts ON {$wpdb->usermeta}.meta_value = {$wpdb->posts}.ID WHERE {$wpdb->usermeta}.user_id = '%d' AND {$wpdb->usermeta}.meta_key = 'family'",
                    $ID
                )
            );

            if (empty($count)) {
                $this->familyId = null;
                $this->save();
            }
        }

        $args = [
            'post_status' => 'publish',
            'post_type'   => 'wallet',
            'author'      => $ID,
        ];

        $query = new WP_Query($args);

        if ($query->have_posts()) {
            foreach ($query->posts as $wallet) {
                $this->wallets[] = $wallet->ID;
            }
        }
    }

    /**
     * Ger instance of Member by ID.
     *
     * @param int $ID       Member ID.
     *
     * @return static
     */
    public static function getInstance(int $ID): self
    {
        if (! empty(static::$instances[$ID])) {
            return static::$instances[$ID];
        }

        static::$instances[$ID] = new static($ID);

        return static::$instances[$ID];
    }

    /**
     * Get member name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get member ID.
     *
     * @return int
     */
    public function getID(): int
    {
        return $this->ID;
    }

    /**
     * Get user accounts IDs.
     *
     * @param string $context       Calling context should be 'show' or 'use'.
     *
     * @return array
     */
    public function getWallets(string $context = 'show'): array
    {
        if (empty($this->wallets) || ! Family::getInstance($this->familyId)->isMember($this->ID)) {
            return [];
        }
        $accounts = [];

        // If it's a current user, return all accounts.
        if ($this->ID === get_current_user_id()) {
            foreach ($this->wallets as $id) {
                $type = get_post_meta($id, 'type', true);

                if ('credit' === $type) {
                    $accounts[ $id ] = CreditCard::getInstance($id);
                } else {
                    $accounts[ $id ] = Wallet::getInstance($id);
                }
            }

            return $accounts;
        }

        foreach ($this->wallets as $id) {
            $type = get_post_meta($id, 'type', true);

            if ('credit' === $type) {
                $account = CreditCard::getInstance($id);
            } else {
                $account = Wallet::getInstance($id);
            }

            switch ($context) {
                case 'show':
                    if ($account->isVisible()) {
                        $accounts[ $id ] = $account;
                    }
                    break;
                case 'use':
                    if ($account->isUsage()) {
                        $accounts[ $id ] = $account;
                    }
                    break;
            }
        }

        return $accounts;
    }

    /**
     * Get member account by account ID.
     *
     * @param int $accountID        Account ID.
     * @param string $context       Calling context should be 'show' or 'use'.
     *
     * @return Wallet|null
     */
    public function getWallet(int $accountID, string $context = 'show'): ?Wallet
    {
        if (0 === $accountID
            || ! Family::getInstance($this->familyId)->isMember($this->ID)
            || ! array_key_exists($accountID, Family::getInstance($this->familyId)->getWallets())
        ) {
            return null;
        }

        $wallet_type = get_post_meta($accountID, 'type', true);

        if ('credit' === $wallet_type) {
            $account = CreditCard::getInstance($accountID);
        } else {
            $account = Wallet::getInstance($accountID);
        }

        // If it's a current user, return all account.
        if ($this->ID === get_current_user_id()) {
            return $account;
        }

        switch ($context) {
            case 'show':
                if (! $account->isVisible()) {
                    return null;
                }
                break;
            case 'use':
                if (! $account->isUsage()) {
                    return null;
                }
                break;
        }

        return $account;
    }

    /**
     * Get member family instance.
     *
     * @return Family
     */
    public function getFamily(): ?Family
    {
        if (empty($this->familyId)) {
            return null;
        }

        return Family::getInstance($this->familyId);
    }

    /**
     * Create new account.
     *
     * @param array $accountData        Account data consist of fields: title, balance, limit, type, visible, used .
     *
     * @return bool                     Result.
     */
    public function createWallet(array $accountData): bool
    {
        // Check user before account creation.
        if (! current_user_can('manage_options') && $this->ID !== get_current_user_id()) {
            return false;
        }

        $data = [
            'title' => 'Wallet',
            'balance' => 0,
            'limit'   => 0,
            'type'    => 'regular',
            'visible' => true,
            'used'    => false,
        ];

        $data = array_merge($data, $accountData);

        switch ($data['type']) {
            case 'credit':
                $account = CreditCard::createWallet($data['title'], $this->ID);
                break;
            case 'regular':
            case 'deposit':
                $account = Wallet::createWallet($data['title'], $this->ID);
                break;
        }

        if (! empty($account)) {
            $account->setTitle($data['title']);
            $account->setBalance($data['balance']);
            $account->setFamilyVisibilityStatus($data['visible']);
            $account->setFamilyUsageStatus($data['used']);

            if ('credit' === $data['type']) {
                $account->setCreditLimit(floatval($data['limit']));
            }

            $account->save();
            $this->wallets[$account->getId()] = $account;

            return true;
        }

        return false;
    }

    /**
     * Delete account of current member.
     *
     * @param int $accountId        ID of user account.
     *
     * @return bool                 Result.
     */
    public function deleteWallet(int $accountId): bool
    {
        // Check user before account creation.
        if ($this->ID !== get_current_user_id()) {
            return false;
        }

        if (! empty($this->wallets[$accountId])) {
            unset($this->wallets[$accountId]);

            return true;
        }

        return false;
    }

    /**
     * Add user to family by family ID.
     *
     * @param int $familyId
     *
     * @return bool
     */
    public function setFamily(int $familyId): bool
    {
        if (empty($this->familyId)
            && (
                get_current_user_id() === $this->ID
                || current_user_can('manage_options')
            )
        ) {
            $this->familyId = $familyId;
            $this->save();

            return true;
        }

        return false;
    }

    /**
     * Delete user from current family.
     *
     * @return bool
     */
    public function leaveFamily(): bool
    {
        if (! Family::getInstance($this->familyId)->isMember(get_current_user_id())
            && ! current_user_can('manage_options')
        ) {
            return false;
        }

        $this->familyId = null;
        $this->save();

        return true;
    }

    /**
     * Save member meta.
     */
    public function save(): void
    {
        update_user_meta($this->ID, 'family', $this->familyId);
    }
}
