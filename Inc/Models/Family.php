<?php

namespace Hacc\Inc\Models;

use WP_User_Query;

/**
 * Class Family
 *
 * @author Rodkin Yevhenii <rodkin.yevhenii@gmail.com>
 * @package Hacc\Inc\Models
 */
class Family extends Post
{
    /**
     * Self instance;
     *
     * @var self
     */
    protected static $instance;

    /**
     * Family members.
     *
     * @var array
     */
    protected $members;

    /**
     * Family constructor.
     *
     * @param int $ID       Family id.
     */
    protected function __construct(int $ID)
    {
        parent::__construct($ID);

        $args = [
            'meta_key' => 'family',
            'meta_value' => $ID,
        ];

        $query = new WP_User_Query($args);

        if (0 < $query->get_total()) {
            foreach ($query->get_results() as $user) {
                $this->members[$user->ID] = $user->ID;
            }
        }
    }

    /**
     * Get self instance.
     *
     * @param int $ID       Family ID.
     *
     * @return static
     */
    public static function getInstance(int $ID): self
    {
        if (! empty(static::$instance)) {
            return static::$instance;
        }

        static::$instance = new static($ID);

        return static::$instance;
    }

    /**
     * Create new family.
     *
     * @param string $title             Family title.
     * @param int    $memberID          Member ID.
     *
     * @return bool
     */
    public static function createFamily(string $title, int $memberID = 0): ?bool
    {
        $member_id = empty($memberID) ? get_current_user_id() : $memberID;
        $member    = Member::getInstance($member_id);

        if ($member->getFamily()) {
            return false;
        }

        $post_data = [
            'post_title'  => $title,
            'post_status' => 'publish',
            'post_type'   => 'family',
            'post_author' => empty($memberID) ? get_current_user_id() : $memberID,
        ];

        $familyID = wp_insert_post($post_data, true);

        if (is_wp_error($familyID)) {
            return null;
        }

        $member->setFamily($familyID);

        return true;
    }

    /**
     * Delete family.
     *
     * @return bool
     */
    public function deleteFamily(): bool
    {
        if (! $this->isMember(get_current_user_id())) {
            return false;
        }

        if (! empty($this->members)) {
            foreach ($this->getMembers() as $member) {
                /** @var Member $member */
                $member->leaveFamily();
            }
        }

        $result = wp_delete_post($this->id);

        return (bool) $result;
    }

    /**
     * Get family members.
     *
     * @return array        Members list.
     */
    public function getMembers() : array
    {
        if (! empty($this->members)) {
            $members = [];

            foreach ($this->members as $id) {
                $members[$id] = Member::getInstance($id);
            }

            return $members;
        }

        return [];
    }

    /**
     * Get wallets of family members.
     *
     * Send "show" context if you want to show wallets for transfer money
     * to this wallets.
     * Send "use" context if you want to transfer withdraw money from
     * this wallets.
     *
     * @param string $context       Calling context should be 'show' or 'use'.
     *
     * @return array|null
     */
    public function getWallets(string $context = 'show'): ?array
    {
        if (! in_array($context, ['show', 'use'])) {
            return null;
        }

        $wallets = [];

        foreach ($this->getMembers() as $member) {
            /** @var Member $member */
            foreach ($member->getWallets($context) as $wallet) {
                /** @var Wallet $wallet */
                $wallets[$wallet->getId()] = $wallet;
            }
        }

        return $wallets;
    }

    /**
     * Add new member to family.
     *
     * @param int $id       User id.
     *
     * @return bool         Result
     */
    public function addMember(int $id) : bool
    {
        if (! current_user_can('manage_options')
            && !$this->isMember(get_current_user_id())
        ) {
            return false;
        }

        if (in_array($id, $this->members)) {
            return true;
        }

        $user = Member::getInstance($id);

        if ($user) {
            $user->setFamily($this->id);
            $this->members[] = $user->getID();

            return true;
        }

        return false;
    }

    /**
     * Remove user from members list.
     *
     * @param int $id       User ID.
     *
     * @return bool
     */
    public function removeMember(int $id): bool
    {
        if (! current_user_can('manage_options')
            && !$this->isMember(get_current_user_id())
        ) {
            return false;
        }

        if (! empty($this->members) && is_array($this->members) && array_key_exists($id, $this->members)) {
            $member = Member::getInstance($id);

            if ($member && $member->leaveFamily()) {
                unset($this->members[ $id ]);

                return true;
            }
        }

        return false;
    }

    /**
     * Check connection between user and family.
     *
     * @param int $userId       User ID.
     *
     * @return bool             Checking result.
     */
    public function isMember(int $userId): bool
    {
        if (in_array($userId, $this->members)) {
            return true;
        }

        return false;
    }

    /**
     * Update post.
     */
    public function save(): ?int
    {
        $post_data = [
            'ID'          => $this->id,
            'post_title'  => $this->title,
        ];

        $post_id = wp_update_post($post_data, true);

        if (is_wp_error($post_id)) {
            return null;
        }

        $members_ids = array_keys($this->members);

        update_post_meta($this->id, 'members', $members_ids);

        return $post_id;
    }
}
