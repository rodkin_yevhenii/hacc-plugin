<?php

namespace Hacc\Inc\Api\Callbacks;

use Hacc\Inc\Models\Wallet;
use Hacc\Inc\Models\Member;

/**
 * Class AjaxCallbacks
 *
 * @package Rodkin Yevheii <rodkin.yevhenii@gmail.com>
 * @package Hacc\Inc\Api\Callbacks
 */
class AjaxCallbacks
{
    /**
     * Display wallet balance html and return balance data.
     */
    public static function displayWalletBalanceHtml()
    {
        $id     = (int) $_POST['wallet_id'];
        $member = Member::getInstance(get_current_user_id());

        ob_start(); ?>
        <ul id="wallet-info">
            <li>
                <span class="title"><?= __('Balance', 'hacc') ?>:</span>
                <span class="amount">
                    <?= $member->getWallet($id)->getBalance(); ?>
                    <small><?= __('UAH', 'hacc') ?></small>
                </span>
            </li>
            <?php if ('credit' === $member->getWallet($id)->getType()) : ?>
                <li>
                    <span class="title"><?= __('Credit limit', 'hacc') ?>:</span>
                    <span class="amount">
                        <?= $member->getWallet($id)->getCreditLimit(); ?>
                        <small><?= __('UAH', 'hacc') ?></small>
                    </span>
                </li>
                <li>
                    <span class="title"><?= __('Own money', 'hacc') ?>:</span>
                    <span class="amount">
                        <?= $member->getWallet($id)->getOwnMoney(); ?>
                        <small><?= __('UAH', 'hacc') ?></small>
                    </span>
                </li>
            <?php endif; ?>
        </ul>
        <?php
        $html = ob_get_clean();
        $wallet['balance'] = $member->getWallet($id)->getBalance();

        if ('credit' === $member->getWallet($id)->getType()) {
            $wallet['limit']    = $member->getWallet($id)->getCreditLimit();
            $wallet['ownMoney'] = $member->getWallet($id)->getOwnMoney();
        }

        $response = [
            'success' => true,
            'wallet'  => $wallet,
            'html'    => $html,
        ];

        echo wp_json_encode($response);
        wp_die();
    }

    /**
     * Create new wallet.
     */
    public static function createWallet()
    {
        if (! isset($_POST['wallet'])) {
            wp_die();
        }

        $wallet = $_POST['wallet'];
        $data = [
            'title'   => empty($wallet['title']) ? 'Wallet' : sanitize_text_field($wallet['title']),
            'balance' => (float) $wallet['balance'] ?? 0,
            'limit'   => (float) $wallet['limit'] ?? 0,
            'type'    => sanitize_text_field($wallet['type']) ?? 'regular',
            'visible' => 'false' === $wallet['visibility'] ? 0 : 1,
            'used'    => 'false' === $wallet['usage'] ? 0 : 1,
            'owner'   => (int) $wallet['owner'],
        ];

        if (current_user_can('manage_options') && ! empty($data['owner'])) {
            $member = Member::getInstance($data['owner']);
        } else {
            $member = Member::getInstance(get_current_user_id());
        }
        $result = $member->createWallet($data);

        if ($result) {
            echo wp_json_encode(['success' => true]);
        } else {
            echo wp_json_encode(['success' => false]);
        }

        wp_die();
    }

    /**
     * Delete wallet.
     */
    public static function deleteWallet()
    {
        if (! isset($_POST['wallet_id'])) {
            echo wp_json_encode(['success' => false]);
            wp_die();
        }

        $id      = (int) $_POST['wallet_id'];
        $wallet  =  Wallet::getInstance($id);
        $success = $wallet->deleteWallet();

        echo wp_json_encode(['success' => $success]);
        wp_die();
    }

    /**
     * Transfer money
     */
    public static function transferMoney()
    {
        $from_card_id    = (int) $_POST['from_card_id'];
        $to_card_id      = (int) $_POST['to_card_id'];
        $amount          = round(floatval($_POST['amount']), 2);
        $from_card       = Wallet::getInstance($from_card_id);
        $to_card         = Wallet::getInstance($to_card_id);
        $withdraw_result = $from_card->withdrawMoney($amount);

        if (! $withdraw_result) {
            echo wp_json_encode(['success' => false]);
            wp_die();
        }

        $recharge_result = $to_card->rechargeBalance($amount);

        if (! $recharge_result) {
            $from_card->rechargeBalance($amount);
            $from_card->save();

            echo wp_json_encode(['success' => false]);
            wp_die();
        }

        $from_card->save();
        $to_card->save();

        echo wp_json_encode(['success' => true]);
        wp_die();
    }
}
