<?php

namespace Hacc\Inc\Api\Callbacks;

use Hacc\Inc\Base\BaseController;

/**
 * Class AdminCallbacks
 *
 * @author Rodkin Yevhenii
 * @package Hacc\Inc\Api\Callbacks
 */
class AdminCallbacks extends BaseController
{
    /**
     * Include dashboard template.
     *
     * @return string
     */
    public static function adminDashboard()
    {
        return require_once(self::getPluginPath() . "/templates/admin/dashboard.php");
    }

    /**
     * Include family template.
     *
     * @return string
     */
    public static function adminFamily()
    {
        return require_once(self::getPluginPath() . "/templates/admin/family.php");
    }

    /**
     * Include accounts template.
     *
     * @return string
     */
    public static function adminAccounts()
    {
        return require_once(self::getPluginPath() . "/templates/admin/accounts.php");
    }
}
