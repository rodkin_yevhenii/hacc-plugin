jQuery(function ($) {

    /**
     * Main wallet tabs.
     */
    $('.list-group-js li').on('click', function (e) {
        e.preventDefault();
        let list_group_js = $(this).parent('.list-group-js');
        list_group_js.find('li').removeClass('active');
        $(this).tab('show');
    });

    /**
     * Show credit limit field in create wallet form.
     */
    let create_wallet_form = $('#create-wallet-form');
    create_wallet_form.find('.js_new-wallet-type input').on('click', function () {
        create_wallet_form.find('.js_credit_limit').hide(200);

        if ($(this).attr('id') === 'newCreditWallet') {
            create_wallet_form.find('.js_credit_limit').show(200);
        }
    });

    /**
     * Ajax action of load balance html.
     */
    $('.js_wallets_list .js_wallet').on('click', function () {
        let wallet_id = $(this).attr('id');
        ajaxLoadBalanceHtml(wallet_id);
    });

    $('#create_wallet').on('click', function (e) {
        e.preventDefault();
        ajaxCreateWallet();
    });

    /**
     * FUNCTIONS
     */

    /**
     * Ajax loading of balance html.
     *
     * @param wallet_id
     */
    function ajaxLoadBalanceHtml(wallet_id)
    {
        let info = $('#collapseInfo .card-body');

        $.ajax({
            url: ajaxurl,
            type: 'Post',
            dataType: 'json',
            data: {
                action: 'display_wallet_balance_html',
                wallet_id: wallet_id,
            },
            beforeSend: function () {
                let height = info.height();
                info.html(
                    '<div class="d-flex justify-content-center align-items-center js_spinner">\n' +
                    '  <div class="spinner-border" role="status">\n' +
                    '    <span class="sr-only">Loading...</span>\n' +
                    '  </div>\n' +
                    '</div>'
                );

                info.find('.js_spinner').height(height);
                $('.js_wallets_list .js_wallet.active').find('.balance').html(
                    '<div class="spinner-border spinner-border-sm" role="status">\n' +
                    '  <span class="sr-only">Loading...</span>\n' +
                    '</div>'
                );
            },
            success: function (response) {
                if (response.success) {
                    info.html(response.html);
                    $('.js_wallets_list .js_wallet.active').find('.balance').html(response.wallet.balance);
                }
            }
        });
    }

    function ajaxCreateWallet()
    {
        let form       = $('#create-wallet-form');
        let title      = $('#newWalletTitle').val();
        let balance    = $('#newWalletBalance').val();
        let owner      = $('#newWalletOwner').val();
        let type       = form.find('input[name=type]:checked').val();
        let limit      = $('#newWalletLimit').val();
        let visibility = form.find('input[name=visibility]:checked').val();
        let usage      = form.find('input[name=usage]:checked').val();

        if (0 === title.length) {
            $('#create-wallet-error').show();
            return;
        }

        let data = {
            action: 'create_wallet',
            wallet: {
                title,
                balance,
                owner,
                type,
                limit,
                visibility,
                usage,
            }
        };

        $.ajax({
            url: ajaxurl,
            type: 'Post',
            dataType: 'json',
            data: data,
            beforeSend: function () {
                $('#create_wallet .spinner-grow').show();
            },
            success: function (response) {
                if (response.success) {
                    $('#create-wallet-success').show();
                } else {
                    $('#create-wallet-error').show();
                }

                $('#create_wallet .spinner-grow').hide();
            }
        });
    }
});
