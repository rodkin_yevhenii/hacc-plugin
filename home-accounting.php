<?php
/**
 * Main file of "Home accounting" plugin
 *
 * @author        Rodkin Evhenii
 * @package       Hacc
 */

/*
Plugin Name: Home Accounting
Version: 0.0.1
Text Domain: hacc
*/

// If this file is called directly, abort!!!
defined('ABSPATH') || die('Access denied!');

// Require once the Composer Autoload.
if (file_exists(dirname(__FILE__) . '/vendor/autoload.php')) {
    require_once dirname(__FILE__) . '/vendor/autoload.php';
}

// Initialize all classes.
if (file_exists(dirname(__FILE__) . '/Inc/Init.php')) {
    Hacc\Inc\Init::registerServices();
}
